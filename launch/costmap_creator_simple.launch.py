import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
   config = os.path.join(
      get_package_share_directory('pointcloud_to_costmap'),
      'config',
      'params_simple.yaml'
   )

   return LaunchDescription([
      Node(
         package='pointcloud_to_costmap',
         executable='pointcloud_to_costmap_node',
         namespace='',
         name='pointcloud_to_costmap',
         parameters=[config]
      )
   ])
