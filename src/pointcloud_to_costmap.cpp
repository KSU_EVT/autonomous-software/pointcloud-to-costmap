#include "pointcloud_to_costmap.hpp"
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/crop_box.h>

PointCloudToCostmap::PointCloudToCostmap() : Node("pointcloud_to_costmap") {
    loadParameters();
    subscribeToTopics();
    publishToTopics();
}

void PointCloudToCostmap::loadParameters(){
    cloud_topic_name_ = this->declare_parameter<std::string>("cloud_topic_name");
    RCLCPP_INFO_STREAM(this->get_logger(), "cloud_topic_name: " << cloud_topic_name_);
    cells_per_meter_ = this->declare_parameter<double>("cells_per_meter");
    RCLCPP_INFO_STREAM(this->get_logger(), "cells_per_meter: " << cells_per_meter_);
    costmap_topic_name_ = this->declare_parameter<std::string>("costmap_topic_name");
    RCLCPP_INFO_STREAM(this->get_logger(), "costmap_topic_name: " << costmap_topic_name_);
    frame_id_ = this->declare_parameter<std::string>("frame_id");
    RCLCPP_INFO_STREAM(this->get_logger(), "frame_id: " << frame_id_);
    grid_x_ = this->declare_parameter<double>("grid_x");
    RCLCPP_INFO_STREAM(this->get_logger(), "grid_x: " << grid_x_);
    grid_y_ = this->declare_parameter<double>("grid_y");
    RCLCPP_INFO_STREAM(this->get_logger(), "grid_y: " << grid_y_);

    // dilation_coefficient_ = this->declare_parameter<double>("dilation_coefficient");
    // RCLCPP_INFO_STREAM(this->get_logger(), "dilation_coefficient: " << dilation_coefficient_);
    dilation_coefficient_x_ = this->declare_parameter<double>("dilation_coefficient_x");
    RCLCPP_INFO_STREAM(this->get_logger(), "dilation_coefficient_x: " << dilation_coefficient_x_);
    dilation_coefficient_y_ = this->declare_parameter<double>("dilation_coefficient_y");
    RCLCPP_INFO_STREAM(this->get_logger(), "dilation_coefficient_y: " << dilation_coefficient_y_);

    x_offset_ = this->declare_parameter<double>("x_offset");
    RCLCPP_INFO_STREAM(this->get_logger(), "x_offset: " << x_offset_);
    y_offset_ = this->declare_parameter<double>("y_offset");
    RCLCPP_INFO_STREAM(this->get_logger(), "y_offset: " << y_offset_);

    point_array_topic_name_ = this->declare_parameter<std::string>("point_array_topic_name");
    RCLCPP_INFO_STREAM(this->get_logger(), "point_array_topic_name: " << point_array_topic_name_);
    goal_point_topic_name_ = this->declare_parameter<std::string>("goal_point_topic_name");
    RCLCPP_INFO_STREAM(this->get_logger(), "goal_point_topic_name: " << goal_point_topic_name_);
    check_y_min_ = this->declare_parameter<int>("check_y_min");
    RCLCPP_INFO_STREAM(this->get_logger(), "check_y_min: " << check_y_min_);
    check_y_ = this->declare_parameter<int>("check_y");
    RCLCPP_INFO_STREAM(this->get_logger(), "check_y: " << check_y_);
    check_x_ = this->declare_parameter<int>("check_x");
    RCLCPP_INFO_STREAM(this->get_logger(), "check_x: " << check_x_);

    blur_kernel_size_ = this->declare_parameter<int>("blur_kernel_size");
    RCLCPP_INFO_STREAM(this->get_logger(), "blur_kernel_size: " << blur_kernel_size_);
    blur_sigma_x_ = this->declare_parameter<int>("blur_sigma_x");
    RCLCPP_INFO_STREAM(this->get_logger(), "blur_sigma_x: " << blur_sigma_x_);
    blur_sigma_y_ = this->declare_parameter<int>("blur_sigma_y");
    RCLCPP_INFO_STREAM(this->get_logger(), "blur_sigma_y: " << blur_sigma_y_);
    
}

void PointCloudToCostmap::subscribeToTopics()
{
    RCLCPP_WARN(get_logger(), "subscribing");
    cloud_subscriber_ = this->create_subscription<sensor_msgs::msg::PointCloud2>(
        cloud_topic_name_,
        10,
        std::bind(&PointCloudToCostmap::create_costmap, this, std::placeholders::_1)
    );
    RCLCPP_WARN(get_logger(), "subscribed");
}

void PointCloudToCostmap::publishToTopics() {

    //costmap_publisher_ = this->create_publisher<nav_msgs::msg::OccupancyGrid>(costmap_topic_name_, 10);
    //point_array_publisher_ = this->create_publisher<visualization_msgs::msg::MarkerArray>(point_array_topic_name_, 10);
    goal_point_publisher_ = this->create_publisher<geometry_msgs::msg::Point32>(goal_point_topic_name_, 10);
}

void PointCloudToCostmap::create_costmap(const sensor_msgs::msg::PointCloud2& msg)
{    
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>);
    pcl::fromROSMsg(msg,*cloud); 

    // pcl::getMinMax3D(non_ground_cloud_,min_point_,max_point_);

    double grid_x = grid_x_;
    double grid_y = grid_y_;
    
    // ROS_INFO_STREAM("creating translation matrix");
    // 2 x 3 matrix
    Eigen::MatrixXd translation_matrix(3,3);
    translation_matrix(0,0) = 1.0;
    translation_matrix(1,1) = -1.0;
    translation_matrix(1,0) = 0.0;
    translation_matrix(0,1) = 0.0;
    translation_matrix(0,2) = (grid_x/2);
    translation_matrix(1,2) = (grid_y/2);
    translation_matrix(2,2) = 0.0;
    translation_matrix(2,0) = 0.0;
    translation_matrix(2,1) = 0.0;

    
    costmap_.info.width = (int) std::ceil(cells_per_meter_ * grid_x);
    costmap_.info.height = (int) std::ceil(cells_per_meter_ * grid_y);
    
    //RCLCPP_INFO_STREAM(this->get_logger(), "got costmap width: " << costmap_.info.width );
    //RCLCPP_INFO_STREAM(this->get_logger(), "got costmap height: " << costmap_.info.height);
    
    Eigen::MatrixXd simplified_grid_(costmap_.info.width, costmap_.info.height);
    Eigen::MatrixXd simplified_grid2_(costmap_.info.width, costmap_.info.height);
    
    simplified_grid_ = Eigen::MatrixXd::Constant(costmap_.info.width, costmap_.info.height, 0);
        
    costmap_.info.resolution = 1.0 / cells_per_meter_;

    for (pcl::PointXYZI point : *cloud){

        // offset the x down to lead turns into a race line
        //RCLCPP_INFO_STREAM(this->get_logger(), "point: " << point.x+x_offset_ << " " << point.y+y_offset_);
        Eigen::Vector3d e_vector(point.x+x_offset_,point.y+y_offset_,1.0);

        Eigen::Vector3d translated_point = translation_matrix * e_vector;

        //RCLCPP_INFO_STREAM(this->get_logger(), "floor: " << ((int) std::floor( cells_per_meter_ * translated_point(0) ))  << " " << ((int) std::floor( cells_per_meter_ * translated_point(1) )));
        int floor_row = (int) std::floor( cells_per_meter_ * translated_point(0) );
        int floor_col = (int) std::floor( cells_per_meter_ * translated_point(1) );
        if (floor_row >= 0 && floor_row < simplified_grid_.rows() &&
            floor_col >= 0 && floor_col < simplified_grid_.cols()) {
            simplified_grid_(floor_row, floor_col) = 100;
        }
        //RCLCPP_INFO_STREAM(this->get_logger(), "ceil: " << ((int) std::ceil( cells_per_meter_ * translated_point(0) ))  << " " << ((int) std::ceil( cells_per_meter_ * translated_point(1) )));
        int ceil_row = (int) std::ceil( cells_per_meter_ * translated_point(0) );
        int ceil_col = (int) std::ceil( cells_per_meter_ * translated_point(1) );
        if (ceil_row >= 0 && ceil_row < simplified_grid_.rows() &&
            ceil_col >= 0 && ceil_col < simplified_grid_.cols()) {
            simplified_grid_(ceil_row, ceil_col) = 100;
        }
    }

    cv::Mat cvMat, dilation_dst;
    cv::eigen2cv(simplified_grid_, cvMat);

    cv::Mat element = cv::getStructuringElement( cv::MORPH_ELLIPSE,
                       cv::Size( 2*dilation_coefficient_x_ +1, 2*dilation_coefficient_y_+1),
                       cv::Point( dilation_coefficient_x_, dilation_coefficient_y_ ) );
    
    // Defines a circular area in which a gaussian blur is applied (L Squares)
    // cv::Mat elementCircular = cv::Mat::zeros(element.rows, element.cols, CV_8UC1);
    // cv::circle(elementCircular, cv::Point(dilation_coefficient_x_, dilation_coefficient_y_), dilation_coefficient_x_, cv::Scalar(255), -1, 8, 0);
    // cv::Mat elementFinal = element & elementCircular;

    cv::Mat image_blurred_with_5x5_kernel;

    cv::dilate( cvMat, dilation_dst, element );
    cv::GaussianBlur(dilation_dst, image_blurred_with_5x5_kernel, cv::Size(blur_kernel_size_, blur_kernel_size_), blur_sigma_x_, blur_sigma_y_);    
    cv::cv2eigen(image_blurred_with_5x5_kernel, simplified_grid2_);

    Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> rm(simplified_grid2_);

    std::vector<p> row_goals;
    int n_min = check_y_min_;
    int n = check_y_;
    int n2 = check_x_;

    p initial_point;
    initial_point.x=(cells_per_meter_ * grid_x)/2;
    initial_point.y=(cells_per_meter_ * grid_y)/2;

    visualization_msgs::msg::MarkerArray ma;
    
    // search through the number of rows in front of us
    floatPoint row_point;
    row_point.y=0;
    for (int i =check_y_min_;i<=n;i++)
    {       
        // within each row in front of us, look at the grid spaces to the n2/2 spaces to the left to right spaces in front of us
        std::vector<float> goals_vector;
        for(int j=(-n2/2); j<(n2/2); j++)
        {
            // in our vector we are evaluating we are looking are looking at the point we generated that was "good" in the reference of our simplified grid
            int row = initial_point.x + i;
            int col = ((int)(row_point.y * cells_per_meter_) + initial_point.y) - j;
            if (row >= 0 && row < simplified_grid2_.rows() &&
                col >= 0 && col < simplified_grid2_.cols()) {
                goals_vector.push_back(simplified_grid2_(row, col));   
            }
        }

        row_point.y -= ((float)(getLeastAngleOfCosts(goals_vector, (n2/2)) - (n2/2)) / (float)cells_per_meter_);
        row_point.x = ((float)(i-1)/ (float) cells_per_meter_);

        // ROS_INFO_STREAM("rowpoint x: " << row_point.x << " y: " << row_point.y);
        // ROS_INFO_STREAM("got index of: " << getLeastAngleOfCosts(goals_vector, n2/2));
        // ROS_INFO_STREAM("got index of: " << getLeastAngleOfCosts(goals_vector, n2/2));
        // ROS_INFO_STREAM("got index of: " << getLeastAngleOfCosts(goals_vector, n2/2));        

        // row_goals.push_back(row_point);
        uint32_t shape = visualization_msgs::msg::Marker::SPHERE;
        visualization_msgs::msg::Marker marker;

        marker.header.frame_id = frame_id_;
        marker.header.stamp = this->get_clock()->now();
        marker.id = i;
        marker.ns = "basic_shapes";
        marker.type = shape;
        marker.action = visualization_msgs::msg::Marker::ADD;

        marker.pose.position.z = 0;
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;

        marker.scale.x = 0.1;
        marker.scale.y = 0.10;
        marker.scale.z = 0.10;

        marker.color.r = 0.0f;
        marker.color.g = 1.0f;
        marker.color.b = 0.0f;
        marker.color.a = 1.0;
        marker.lifetime = builtin_interfaces::msg::Duration();
 
        marker.pose.position.x = row_point.x;
        marker.pose.position.y = -row_point.y;

        ma.markers.push_back(marker);
        
        goals_vector.clear();
    }

    geometry_msgs::msg::Point32 goal_point;
    goal_point.x = row_point.x;
    goal_point.y = -row_point.y;
    goal_point_publisher_->publish(goal_point);

    //point_array_publisher_->publish(ma);
    Eigen::Map<Eigen::RowVectorXd> v( rm.data(), rm.size() );
    
    
    costmap_.data.clear();
    for(int i=0; i < v.size(); i++){
        costmap_.data.push_back((int) v(i));        
    }
    
    costmap_.header.frame_id = frame_id_;

    msg_pose_.position.x = -grid_x_/2;
    msg_pose_.position.y = grid_y_/2;
    
    msg_pose_.orientation.w = -0.7071068;
    msg_pose_.orientation.z = 0.7071068;
    costmap_.info.origin = msg_pose_;
    //costmap_publisher_->publish(costmap_);
}

int PointCloudToCostmap::getLeastAngleOfCosts(std::vector<float> row, int row_width)
{
    float least_val = 1000;
    int least_angle_index = 0;
    int least_angle = row_width;
    for(int i = 0, max = row.size(); i < max;i++){
        
        if( row[i] < least_val ){
            least_val=row[i];
        }
        //get least val
    }


    for(int j = 0, max = row.size(); j < max;j++){
        
        // look at the values that are equal to the least value in the grid
        if( row[j] == least_val )
        {
            // the least angle change is the closest least valuegetLeastAngleOfCosts(goals_vector, n2/2) to the center of grid strip
            if(abs((-row_width)+j) < least_angle ){
                least_angle = abs((-row_width)+j);
                least_angle_index=j;
            }
        }    
    }

    return least_angle_index;
}
