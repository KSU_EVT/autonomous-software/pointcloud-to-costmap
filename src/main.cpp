#include <rclcpp/rclcpp.hpp>
#include <pointcloud_to_costmap.hpp>

int main(int argc, char **argv){
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<PointCloudToCostmap>());
    rclcpp::shutdown();
    return 0;
}
