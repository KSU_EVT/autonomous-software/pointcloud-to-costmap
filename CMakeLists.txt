cmake_minimum_required(VERSION 3.8)
project(pointcloud_to_costmap)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

find_package(PCL 1.7 REQUIRED)
find_package(FLANN REQUIRED MODULE) # PCL dep; Not sure why we have to do this

find_package(OpenCV REQUIRED)

find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(nav_msgs REQUIRED)
find_package(visualization_msgs REQUIRED)
find_package(pcl_ros REQUIRED)
find_package(pcl_conversions REQUIRED)

add_executable(pointcloud_to_costmap_node
  src/main.cpp
  src/pointcloud_to_costmap.cpp
)

target_compile_features(pointcloud_to_costmap_node PUBLIC
  c_std_99
  cxx_std_17
)  # Require C99 and C++17

ament_target_dependencies(pointcloud_to_costmap_node PUBLIC
  rclcpp
  nav_msgs
  visualization_msgs
  pcl_ros
  pcl_conversions
)

target_link_libraries(pointcloud_to_costmap_node PUBLIC
  ${OpenCV_LIBS}
  ${PCL_LIBRARIES}
)

target_include_directories(pointcloud_to_costmap_node PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
)

install(TARGETS 
  pointcloud_to_costmap_node
  DESTINATION lib/${PROJECT_NAME}
)
install(DIRECTORY
  include
  DESTINATION include/${PROJECT_NAME}/
)
install(DIRECTORY
  launch
  DESTINATION share/${PROJECT_NAME}/
)
install(DIRECTORY
  config
  DESTINATION share/${PROJECT_NAME}/
)

if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  # the following line skips the linter which checks for copyrights
  # uncomment the line when a copyright and license is not present in all source files
  # the following line skips cpplint (only works in a git repo)
  # uncomment the line when this package is not in a git repo
  #set(ament_cmake_cpplint_FOUND TRUE)
  ament_lint_auto_find_test_dependencies()
endif()

ament_package()