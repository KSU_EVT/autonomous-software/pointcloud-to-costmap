#include <vector>
#include <algorithm>

#include <rclcpp/rclcpp.hpp>

#include <pcl/point_types.h>
#include <pcl/common/common.h> //getMinMax3d()
#include <pcl/filters/voxel_grid.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>

#include <sensor_msgs/msg/point_cloud2.hpp>
#include <nav_msgs/msg/occupancy_grid.hpp>
#include <visualization_msgs/msg/marker_array.hpp>
#include <visualization_msgs/msg/marker.hpp>
#include <geometry_msgs/msg/point32.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>

class PointCloudToCostmap : public rclcpp::Node {

    public:
        PointCloudToCostmap();

    private:

        struct p
        {
            int x;
            int y;
        };
        
        struct floatPoint
        {
            float x;
            float y;
        };

        void loadParameters();
        void subscribeToTopics();
        void publishToTopics();

        void create_costmap(const sensor_msgs::msg::PointCloud2& msg);
        int getLeastAngleOfCosts(std::vector<float> row, int row_width);

        rclcpp::Subscription<sensor_msgs::msg::PointCloud2>::SharedPtr cloud_subscriber_;
        rclcpp::Publisher<nav_msgs::msg::OccupancyGrid>::SharedPtr costmap_publisher_;
        rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr point_array_publisher_;
        rclcpp::Publisher<geometry_msgs::msg::Point32>::SharedPtr goal_point_publisher_;

        double dilation_coefficient_;
        double dilation_coefficient_x_;
        double dilation_coefficient_y_;

        double x_offset_;
        double y_offset_;

        std::string cloud_topic_name_;
        std::string costmap_topic_name_;
        std::string goal_point_topic_name_;
        std::string point_array_topic_name_;
        double cells_per_meter_;

        double grid_x_;
        double grid_y_;

        pcl::PointXYZI min_point_;
        pcl::PointXYZI max_point_;

        std::string frame_id_;

        geometry_msgs::msg::Pose msg_pose_;
        nav_msgs::msg::OccupancyGrid costmap_;

        int check_y_min_;
        int check_y_;
        int check_x_;

        int blur_kernel_size_;
        double blur_sigma_;
        double blur_sigma_x_;
        double blur_sigma_y_;

};
